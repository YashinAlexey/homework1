using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using XmlSerialization;
using Xunit;

namespace XUnitTestHomeWork1
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var mock = new Mock<IRepository<Account>>();
            mock.Setup(x => x.GetAll()).Returns(GetAll());
            mock.Setup(x => x.GetOne(It.IsAny<Func<Account, bool>>())).Returns((Func<Account, bool> x) => GetOne(x));
            mock.Setup(x => x.Add(It.IsNotNull<Account>())).Verifiable();

            var accountController = new AccountController(mock.Object);

            accountController.AddAccount(new Account { FirstName = "Tommy", LastName = "Emmanuel", BirthDate = new DateTime(2020, 10, 1) });

            var account = accountController.Get(x => x.FirstName == "Tommy" && x.LastName == "Emmanuel" && x.BirthDate == new DateTime(2020, 10, 1));

            Assert.NotNull(account);
            Assert.Equal(account.FirstName, "Tommy");
            Assert.Equal(account.LastName, "Emmanuel");
            Assert.Equal(account.BirthDate, new DateTime(2020, 10, 1));
        }

        public IEnumerable<Account> GetAll()
        {
            return new List<Account> { new Account { FirstName = "James", LastName = "Bartholomew", BirthDate = new DateTime(2020, 3, 1) },
                                       new Account { FirstName = "Doc", LastName = "Watson", BirthDate = new DateTime(2020, 5, 1) },
                                       new Account { FirstName = "Gareth", LastName = "Evans", BirthDate = new DateTime(2020, 7, 1)},
                                       new Account { FirstName = "Tommy", LastName = "Emmanuel", BirthDate = new DateTime(2020, 10, 1) }};            
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            var items = GetAll();
            if (items.Count() == 0)
                return null;
            try
            {
                return items.First(predicate);
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }
    }
}
