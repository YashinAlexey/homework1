﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace XmlSerialization
{
	public class StreamReader<T> : IEnumerable<T>, IDisposable where T : class
	{
		private FileStream _stream;
		private XmlSerializer<T> _serializer;
		private bool _isDisposed;

		public StreamReader(FileStream stream, XmlSerializer<T> serializer)
		{
			_stream = stream;
			_serializer = serializer;
		}

		public FileStream FileStream 
		{
			get => _stream;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		public IEnumerator<T> GetEnumerator()
		{
            if (_stream.Length == 0) yield break; 

			var items = _serializer.Deserialize<T>(_stream);
			for (int i = 0; i < items.Length; ++i)
				yield return items[i];
		}

		protected virtual void Dispose(Boolean isManual)
		{
			if (_isDisposed) return;

			if (isManual) _stream.Dispose();

			_isDisposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		~StreamReader()
		{
			Dispose(false);
		}
	}
}
