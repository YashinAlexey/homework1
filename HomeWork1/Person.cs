﻿using System;

namespace XmlSerialization
{
	[Serializable]
	public class Person : IComparable
	{
		public string Name { get; set; }
		public int Age { get; set; }

        int IComparable.CompareTo(object obj)
        {
            if (Age > ((Person)obj).Age)
                return 1;
            if (Age < ((Person)obj).Age)
                return -1;
            return 0;
        }
    }
}
