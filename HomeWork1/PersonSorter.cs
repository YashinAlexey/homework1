﻿using System.Collections.Generic;
using System.Linq;

namespace XmlSerialization
{
    public class PersonSorter<T> : ISorter<StreamReader<T>> where T : class
    {
        public IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems)
        {
            return notSortedItems.OrderBy(i => i);
        }
    }
}
