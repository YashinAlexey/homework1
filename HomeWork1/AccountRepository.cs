﻿using ExtendedXmlSerializer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace XmlSerialization
{
    public class AccountRepository<T> : IRepository<T> where T : class
    {
        public void Add(T item)
        {
            XmlSerializer<T> xmlSerializer = new XmlSerializer<T>();

            using (StreamReader<T> streamReader = new StreamReader<T>(new FileStream("Accounts.xml", FileMode.OpenOrCreate), xmlSerializer))
            {
                var enumerator = streamReader.GetEnumerator();

                Stack<T> stack = new Stack<T>();
                while (enumerator.MoveNext())
                {
                    stack.Push(enumerator.Current);
                }
                stack.Push(item);

                streamReader.FileStream.SetLength(0);
                xmlSerializer.Serialize(streamReader.FileStream, stack.ToArray());
            }
        }

        public IEnumerable<T> GetAll()
        {
            XmlSerializer<T> xmlSerializer = new XmlSerializer<T>();

            using (StreamReader<T> streamReader = new StreamReader<T>(new FileStream("Accounts.xml", FileMode.OpenOrCreate), xmlSerializer))
            {                
                var enumerator = streamReader.GetEnumerator();

                while (enumerator.MoveNext())
                    yield return enumerator.Current;
            }
        }

        public T GetOne(Func<T, bool> predicate)
        {
            var items = GetAll();
            if (items.Count() == 0)
                return null;
            try
            {
                return items.First(predicate); 
            }
            catch(InvalidOperationException)
            {
                return null;
            }            
        }
    }
}
