﻿using System;
using System.IO;

namespace XmlSerialization
{
	class HomeWork1
	{
		static void Main(string[] args)
		{
            Person[] persons = new Person[] { new Person { Name = "Tom", Age = 40 },
											  new Person { Name = "John", Age = 30 },
											  new Person { Name = "Mary", Age = 20 },
											  new Person { Name = "Anna", Age = 50 },
											  new Person { Name = "Jack", Age = 10 }};
           
            XmlSerializer<Person> xmlSerializer = new XmlSerializer<Person>();
			
			// Сериализация в файл Person.xml
			using (FileStream fileStream = new FileStream("Person.xml", FileMode.Create))
			{
				xmlSerializer.Serialize(fileStream, persons);
			}
			
			// Десериализация из файла Person.xml без сортировки
			using (StreamReader<Person> streamReader = new StreamReader<Person>(new FileStream("Person.xml", FileMode.Open), xmlSerializer))
			{
				Console.WriteLine("Без сортировки:");
				foreach (Person person in streamReader)
				{
					Console.WriteLine($"Name = {person.Name}, age = {person.Age}");
				}
			}

			Console.WriteLine();

			// Десериализация из файла Person.xml с сортировкой
			using (StreamReader<Person> streamReader = new StreamReader<Person>(new FileStream("Person.xml", FileMode.Open), xmlSerializer))
			{
				//Сортировка данных
				PersonSorter<Person> personSorter = new PersonSorter<Person>();
				var sortedPersons = personSorter.Sort<Person>(streamReader);
				
				Console.WriteLine("С сортировкой:");
				foreach (Person person in sortedPersons)
				{
					Console.WriteLine($"Name = {person.Name}, age = {person.Age}");
				}
			}
		}		
	}
}