﻿
using System;

namespace XmlSerialization
{
    public class AccountController : IAccountService
    {
        private readonly IRepository<Account> _repository;

        public AccountController(IRepository<Account> repository)
        {
            _repository = repository;
        }

        public void AddAccount(Account account)
        {
            _repository.Add(account);
        }

        public Account Get(Func<Account, bool> predicate)
        {
            return _repository.GetOne(predicate);
        }
    }
}
