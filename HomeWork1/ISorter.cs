﻿using System.Collections.Generic;

namespace XmlSerialization
{
    public interface ISorter<T>
    {
        IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems);
    }
}
