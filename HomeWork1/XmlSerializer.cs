﻿using System.IO;
using System.Text;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.ContentModel;
using ExtendedXmlSerializer.Configuration;
using ExtendedXmlSerializer.ContentModel.Format;

namespace XmlSerialization
{
	public class XmlSerializer<T> : ISerializer<T> where T : class
	{
		public XmlSerializer() { }

		readonly ISerializer<T> _previous;

		public void Serialize<T>(FileStream fileStream, T item)
		{
			IExtendedXmlSerializer serializer = new ConfigurationContainer()
													.UseAutoFormatting()
													.UseOptimizedNamespaces()
													.EnableImplicitTyping(typeof(T))
													.Create();
			string result = serializer.Serialize(item);
			byte[] info = new UTF8Encoding(true).GetBytes(result);
			fileStream.Write(info, 0, info.Length);
		}

		public T[] Deserialize<T>(FileStream fileStream)
		{
			IExtendedXmlSerializer serializer = new ConfigurationContainer()
													.UseAutoFormatting()
													.UseOptimizedNamespaces()
													.EnableImplicitTyping(typeof(T))
													.Create();
			return serializer.Deserialize<T[]>(fileStream);
		}

		public T Get(IFormatReader parameter) => _previous.Get(parameter);

		public void Write(IFormatWriter writer, T instance)
		{
			_previous.Write(writer, instance);
		}
	}
}
